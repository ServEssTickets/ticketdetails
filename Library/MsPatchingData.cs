﻿using System;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Globalization;

namespace Library
{
    public class MsPatchingData
    {
        readonly private string ConnectionString;
        private DataTable dtPatchingData;
        public string logPath;

        public MsPatchingData(string connectionstring)
        {
            ConnectionString = connectionstring;
        }
        public void LoadPatchingCalendarData()
        {
            dtPatchingData = PopulateDataTable(ConnectionString);
        }

        //Reads the first sheet of the Patching Calendar excelsheet then Populates the datatable with the data from it.
        private DataTable PopulateDataTable(string connection)
        {
            OleDbCommand cmdExcel = new OleDbCommand();
            DataTable dt = null;
            try
            {
                OleDbConnection connExcel = new OleDbConnection(connection);
                string SheetName = GetSheetName(connExcel);
                if (!string.IsNullOrEmpty(SheetName))
                {
                    cmdExcel.Connection = connExcel;
                    dt = ReadExcelSheet(SheetName, cmdExcel, connExcel);
                }
            }
            catch(OleDbException ex)
            {
                Log.write("OleDbConnection exception while populating the data table", ex, logPath);
                return null;
            }
            return dt;
        }

        //Gets the name of the first sheet in the Patching Calendar Excelsheet.
        private string GetSheetName(OleDbConnection connExcel)
        {
            try
            {
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                return SheetName;
            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
        }
        //Reads the data from Patching Calendar Excelsheet into the datatable and renames the columns of the databale with simpler column names.
        private DataTable ReadExcelSheet(string sheetName, OleDbCommand cmdExcel, OleDbConnection connExcel)
        {
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            try
            {
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                //Renaming the columns of the data table
                dt.Columns[0].ColumnName = "Patch_Month";
                dt.Columns[1].ColumnName = "Patch_Dev3Test3";
                dt.Columns[2].ColumnName = "Patch_UAT";
                dt.Columns[3].ColumnName = "Patch_ProdGoNoGo";
                dt.Columns[4].ColumnName = "Patch_ProdDev2Test2_Day1";
                dt.Columns[5].ColumnName = "Patch_ProdDev2Test2_Day2";

                connExcel.Close();
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            return dt;
        }
        //It Fetches the MS patching scheduel for the current month
        //It then checks if the notification emails on the current day needs to be sent as per that schedule.
        public void Process()
        {
            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            DataRow currentMonthRow =null;
            Email.MsPatchingLogPath = logPath;

            if (dtPatchingData !=null && dtPatchingData.Rows.Count > 0)
            {
                //Fetch MS patching schedule for current month
                currentMonthRow = (from row in dtPatchingData.AsEnumerable()
                                   where row[0].ToString().Trim() == CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentMonth)
                                   select row).SingleOrDefault();
                if (currentMonthRow != null)
                {
                    try
                    {
                        DateTime patchDateDev3Test3 = new DateTime(currentYear, currentMonth, int.Parse((currentMonthRow[1]).ToString().Trim()));
                        DateTime patchDateUAT = new DateTime(currentYear, currentMonth, int.Parse((currentMonthRow[2]).ToString().Trim()));
                        DateTime patchDateProdDev2Test2 = new DateTime(currentYear, currentMonth, int.Parse((currentMonthRow[4]).ToString().Trim()));
                        CheckPatchingSchedule(patchDateDev3Test3, patchDateUAT, patchDateProdDev2Test2);
                    }
                    catch(ArgumentOutOfRangeException ex)
                    {
                        Log.write("ArgumentOutOfRangeException exception in the Process() method while initializing datetime instances", ex, logPath);
                    }
                }
            }
        }
        /* 
         * This method checks current date against the MS Patching Schedule of current Month. 
         * MS Patching for Dev3,Test3 is on either  2nd or 3rd Thursday of a month, so send notification mail a day prior to it i.e on Wednesday.
         * MS Patching for UAT is on either  2nd or 3rd Sunday of a month, so send notification mail 2 days prior to it i.e on Friday.
         * MS Patching for Prod,Dev2,Test2 is on 4th Monday of a month, so send notification mail 3 days prior to it i.e on Friday.
         */
        public void CheckPatchingSchedule(DateTime dateDev3Test3, DateTime dateUAT, DateTime dateProdDev2Test2)
        {
            try
            {
                if (dateDev3Test3.Subtract(DateTime.Now).Days == 0)
                    Email.SendPatchingEmail(dateDev3Test3, Constants.envDev3Test3,Constants.mspatching,Email.MsPatchingLogPath);//send mail for Dev3, Test3 MS Patching activvity
                if (dateUAT.Subtract(DateTime.Now).Days == 1)
                    Email.SendPatchingEmail(dateUAT, Constants.envUAT, Constants.mspatching,Email.MsPatchingLogPath); //send mail for UAT MS Patching activvity
                if (dateProdDev2Test2.Subtract(DateTime.Now).Days == 2)
                    Email.SendPatchingEmail(dateProdDev2Test2, Constants.envProdDev2Test2, Constants.mspatching, Email.MsPatchingLogPath); //send mail for Prod, Dev2, Test2 MS Patching activvity
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Log.write("ArgumentOutOfRangeException exception in the CheckPatchingSchedule method while checking the date differences", ex, logPath);
            }
        }
    }
}

