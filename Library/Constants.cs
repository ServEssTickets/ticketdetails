﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public static class Constants
    {
        //Environment constants
        public const string envDev3Test3 = "Dev3Test3";
        public const string envUAT = "UAT";
        public const string envProdDev2Test2 = "ProdDev2Test2";

        //Activity constants
        public const string mspatching = "MsPatching";
        public const string nugetpatching = "NugetPatching";
        public const string CCBMail = "CCBMail";
        public const string CCBPublishMail = "CCBPublishMail";
    }
}
