﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class BreachDetails
    {
        readonly private string ConnectionString;
        public string mailTo;
        public string mailCC;
        public string futureBreachMailSubject;
        public string logPath;

        //master tables
        private DataTable dtMasterTable;
        private DataTable dtFutureBreachResults;


        public BreachDetails(string connectionstring)
        {
            ConnectionString = connectionstring;

        }
        public void LoadMasterData()
        {
            dtMasterTable = PopulateDataTable(ConnectionString);
        }

        public void Process()
        {
            try
            {
                Email.ticketStatusLogPath = logPath;
                Log.write("Populating data table", null, logPath);
                Log.write("Generating reports data table", null, logPath);
                GenerateReports();
                Log.write("Sending Emails", null, logPath);
                SendEmail();
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }
        }

        private void GenerateReports()
        {
            DataView view = new DataView(dtMasterTable);
            /* Get Future Breach results*/
            dtFutureBreachResults =
                   view.ToTable("dtFutureBreachResults", false, "Service-Solution", "BreachDateTime (Incident)", "Summary", "Status", "Incident");

            for (int i = 0; i < dtFutureBreachResults.Rows.Count; i++)
            {
                if (!(!string.IsNullOrEmpty(dtFutureBreachResults.Rows[i]["BreachDateTime (Incident)"].ToString()) && Convert.ToDateTime(dtFutureBreachResults.Rows[i]["BreachDateTime (Incident)"]) >= DateTime.Now))
                {
                    dtFutureBreachResults.Rows[i].Delete();
                }
            }
            var results = dtFutureBreachResults.AsEnumerable().OrderBy(x => x.Field<string>("Service-Solution"));
            dtFutureBreachResults = results.Any() ? results.CopyToDataTable() : null;
        }
        private void SendEmail()
        {
            Email.SendEmailForFutureBreach(futureBreachMailSubject, mailTo, mailCC, dtFutureBreachResults);
        }
        private string GetSheetName(OleDbConnection connExcel)
        {
            try
            {
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                return SheetName;


            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
        }

        private DataTable ReadExcelSheet(string sheetName, OleDbCommand cmdExcel, OleDbConnection connExcel)
        {

            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            try
            {
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            return dt;
        }

        private DataTable PopulateDataTable(string connection)
        {
            OleDbConnection connExcel = new OleDbConnection(connection);
            OleDbCommand cmdExcel = new OleDbCommand();
            DataTable dt = null;

            try
            {
                cmdExcel.Connection = connExcel;
                //Get the name of First Sheet
                string SheetName = GetSheetName(connExcel);

                //Read Data from Sheet
                dt = ReadExcelSheet(SheetName, cmdExcel, connExcel);
                return RowFilter(dt, "Status", new string[] { "Assigned", "In Progress", "Logged", "Received", "Waiting for 3rd Party", "Waiting for Change", "Waiting for Problem Resolution" });
                //return dt;

            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while populating the data table", ex, logPath);
                return null;
            }


        }

        //Filtering the table based on column name(Status)
        public static DataTable RowFilter(DataTable dt, string columnName, string[] FilterValue)
        {
            List<DataRow> val = new List<DataRow>();
            DataTable newDataTable = dt.Clone();
            foreach (var item in FilterValue)
            {
                val.AddRange(dt.AsEnumerable().Where(p => p[columnName].ToString().Equals(item)).ToList());
            }

            foreach (DataRow dr in val)
            {
                newDataTable.ImportRow(dr);
            }
            newDataTable.AcceptChanges();
            Console.WriteLine("Filteration completed based on {0} column", columnName);
            return newDataTable;
        }

    }
}
