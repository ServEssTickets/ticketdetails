﻿namespace pckg_version
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExportNuget = new System.Windows.Forms.Button();
            this.PackageVersions = new System.Windows.Forms.DataGridView();
            this.PackageId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstalledVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AvailableVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.outputmsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PackageVersions)).BeginInit();
            this.SuspendLayout();
            // 
            // ExportNuget
            // 
            this.ExportNuget.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportNuget.Location = new System.Drawing.Point(511, 27);
            this.ExportNuget.Name = "ExportNuget";
            this.ExportNuget.Size = new System.Drawing.Size(189, 23);
            this.ExportNuget.TabIndex = 0;
            this.ExportNuget.Text = "Export NugetList";
            this.ExportNuget.UseVisualStyleBackColor = true;
            this.ExportNuget.Click += new System.EventHandler(this.ExportNuget_Click);
            // 
            // PackageVersions
            // 
            this.PackageVersions.AllowUserToOrderColumns = true;
            this.PackageVersions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PackageVersions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PackageVersions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PackageId,
            this.InstalledVersion,
            this.AvailableVersion});
            this.PackageVersions.Location = new System.Drawing.Point(43, 84);
            this.PackageVersions.Name = "PackageVersions";
            this.PackageVersions.ReadOnly = true;
            this.PackageVersions.RowHeadersVisible = false;
            this.PackageVersions.Size = new System.Drawing.Size(746, 199);
            this.PackageVersions.TabIndex = 1;
            // 
            // PackageId
            // 
            this.PackageId.HeaderText = "Package ID";
            this.PackageId.Name = "PackageId";
            this.PackageId.ReadOnly = true;
            // 
            // InstalledVersion
            // 
            this.InstalledVersion.HeaderText = "Installed Version";
            this.InstalledVersion.Name = "InstalledVersion";
            this.InstalledVersion.ReadOnly = true;
            // 
            // AvailableVersion
            // 
            this.AvailableVersion.HeaderText = "Available Version";
            this.AvailableVersion.Name = "AvailableVersion";
            this.AvailableVersion.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(82, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(189, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Select Root Folder of Solution";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SelectFolder_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // outputmsg
            // 
            this.outputmsg.AutoSize = true;
            this.outputmsg.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputmsg.Location = new System.Drawing.Point(79, 68);
            this.outputmsg.Name = "outputmsg";
            this.outputmsg.Size = new System.Drawing.Size(52, 17);
            this.outputmsg.TabIndex = 3;
            this.outputmsg.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(856, 311);
            this.Controls.Add(this.outputmsg);
            this.Controls.Add(this.ExportNuget);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.PackageVersions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nuget Package List";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PackageVersions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExportNuget;
        private System.Windows.Forms.DataGridView PackageVersions;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackageId;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstalledVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn AvailableVersion;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label outputmsg;
    }
}

