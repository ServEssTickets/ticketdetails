﻿using Library;
using System;
using System.Configuration;
using System.IO;
using System.Linq;

namespace TicketDetails
{
    public class Helper
    {
        static string logPath = ConfigurationManager.AppSettings["Log"].ToString();
        public static void ExecuteExcelFile(string conStr, string directory, string masterDirectory)
        {            
            try
            {
                string[] files;
                string[] masterFiles;
                string connectionString = "";
                string masterConnectionString = "";
                
                if (!Directory.Exists(directory))
                {
                    Log.write("Directory/excel files does not exist", null, logPath);
                    return;
                }
                files = Directory.GetFiles(directory, @"*.xls", SearchOption.TopDirectoryOnly);
                masterFiles = Directory.GetFiles(masterDirectory, @"*.xls", SearchOption.TopDirectoryOnly);
                if (files.Count() == 1)
                    connectionString = string.Format(conStr, files[0]);

                if (masterFiles.Count() == 1)
                    masterConnectionString = string.Format(conStr, masterFiles[0]);

                TicketData objTicketData = new TicketData(connectionString, masterConnectionString);

                //setup mail details
                objTicketData.MailTo = ConfigurationManager.AppSettings["MailTo"].ToString();
                objTicketData.MailCc = ConfigurationManager.AppSettings["MailCc"].ToString();
                objTicketData.IncidentMailSubject = ConfigurationManager.AppSettings["IncidentMailSubject"].ToString();
                objTicketData.ServiceRequestMailSubject = ConfigurationManager.AppSettings["ServiceRequestMailSubject"].ToString();
                objTicketData.ProblemMailSubject = ConfigurationManager.AppSettings["ProblemMailSubject"].ToString();
                objTicketData.LogPath = logPath;
                //Setup master records
                Log.write("Loading master data", null, logPath);
                objTicketData.LoadMasterData();

                //start processing
                Log.write("Processing of the excel file started", null, logPath);
                objTicketData.Process();                
            }
            catch (Exception ex)
            {                
                Log.write("ERROR: ", ex, logPath);
            }
        }

        internal static void ArchiveFile(string directory, string archiveDir)
        {
            try
            {
                Log.write("Archiving of the excel file started", null, logPath);
                if (System.IO.Directory.Exists(directory))
                {
                    string[] files = System.IO.Directory.GetFiles(directory);

                    foreach (string s in files)
                    {
                        string fileName = Path.GetFileName(s);
                        string destFile = Path.Combine(archiveDir+"\\",  Guid.NewGuid().ToString()+"_"+ fileName);
                        File.Move(s, destFile);
                    }
                }
            }
            catch(Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }
            
            
        }
    }
}
